# Day 1 of Advent of Code 2022 ###############################################

See <https://adventofcode.com/2022/day/1/>.

Usage:

~~~~sh
./solve sample-input
# --> 24000
~~~~
