class Knot:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return '({}, {})'.format(self.x, self.y)

    def copy(self):
        return Knot(self.x, self.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def move(self, symb, step_num = 1):
        if   symb == 'U':
            self.y = self.y - step_num
        elif symb == 'D':
            self.y = self.y + step_num
        elif symb == 'L':
            self.x = self.x - step_num
        elif symb == 'R':
            self.x = self.x + step_num
        else:
            raise Exception('unknown direction symbol `{}`'.format(symb))
        return self

    def is_close_to(self, other):
        return abs(self.x - other.x) <= 1 and abs(self.y - other.y) <= 1

    def snap_to(self, other):
        if not self.is_close_to(other):
            self.x = other.x + int((self.x - other.x)/2)
            self.y = other.y + int((self.y - other.y)/2)
        return self

    def cw_max(self, other):
        '''Component-wise maximum'''
        if self.x < other.x:
            self.x = other.x
        if self.y < other.y:
            self.y = other.y

    def cw_min(self, other):
        '''Component-wise maximum'''
        if other.x < self.x:
            self.x = other.x
        if other.y < self.y:
            self.y = other.y


class Grid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.rows = [ ['.'] * width for i in range(height) ]

    def render(self):
        as_str = ''
        for row in self.rows:
            for symb in row:
                as_str = as_str + symb
            as_str = as_str + '\n'
        return as_str[:-1]

    def mark(self, x, y, symb):
        self.rows[y][x] = symb


class Rope:
    def __init__(self, knot_num, start):
        self.knot_num = knot_num
        self.start = start
        self.reset()

    def head(self):
        return self.knots[0]

    def tail(self):
        return self.knots[-1]

    def reset(self):
        self.knots = [ self.start.copy() for i in range(self.knot_num) ]

    def move_head(self, symb, step_num = 1):
        for step in range(step_num):
            self.head().move(symb)
            for index in range(1, len(self.knots)):
                self.knots[index].snap_to(self.knots[index - 1])


import re

class Scene:
    def __init__(self, knot_num, head_motions = None):
        self.knot_num = knot_num
        if head_motions != None:
            self.head_motions = head_motions
            self._calc_area_and_rope_start()
        else:
            self.head_motions = []

    def load_head_motions(self, input_file_path):
        self.head_motions = []
        with open(input_file_path, 'r') as input_file:
            for line in input_file.readlines():
                match = re.fullmatch('([RULD])\s+([0-9]+)', line.strip())
                symb = match.group(1)
                step_num = int(match.group(2))
                self.head_motions.append([symb, step_num])
        self._calc_area_and_rope_start()

    def _calc_area_and_rope_start(self):
        '''Calculates width and height of the ropes area of action as well as
        its start position'''

        # First, we determine the coordinates of the rectangle where the start
        # point is (0, 0)
        #
        head = Knot(0, 0)
        tl = Knot(0, 0)  # top-left corner of rectangular
        br = Knot(0, 0)  # bottom-right corner
        for motion in self.head_motions:
            head.move(motion[0], motion[1])
            tl.cw_min(head)
            br.cw_max(head)

        # From these parameters we derive a coordinate system with the
        # top-left corner of the area be (0, 0), y-axis pointing downwards.
        # (i.e. usual screen coordinates)
        #
        self.width  = br.x - tl.x + 1
        self.height = br.y - tl.y + 1
        self.rope = Rope(self.knot_num, Knot(-tl.x, -tl.y))

    def trace_tail(self):
        grid = Grid(self.width, self.height)
        self.rope.reset()
        for motion in self.head_motions:
            step_num = motion[1]
            symb     = motion[0]
            for _ in range(step_num):
                self.rope.move_head(symb)
                tail = self.rope.tail()
                grid.mark(tail.x, tail.y, '#')
        grid.mark(self.rope.start.x, self.rope.start.y, 's')
        return grid.render()

    def unique_tail_positions(self):
        rendering = self.trace_tail()
        count = 0
        for char in rendering:
            if char == '#' or char == 's':
                count = count + 1
        return count

