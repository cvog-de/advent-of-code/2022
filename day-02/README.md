# Day 2 of Advent of Code 2022 ###############################################

See <https://adventofcode.com/2022/day/2>.

Usage:

~~~~sh
./solve-part-1 sample-input
# --> 15

./solve-part-2 sample-input
# --> 12
~~~~
